<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head>
 *
 * @package WordPress
 * @subpackage Gasoline.Digital.Theme
 * @since Gasoline.Digital.Theme 1.0
 */
	wp_footer(); 
?>
	</body>
</html>