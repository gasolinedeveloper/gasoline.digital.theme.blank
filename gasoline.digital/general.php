<?php
/*
*
* Functions File
*
* @package Gasoline.Digital.Functions
* @subpackage General Functions
* @since Gasoline.Digital.Functions 1.0
*/

/*
* Gasoline.Digital Functions Themes
*
* General Functions
*/
global $mysql_debug, $post_thumbnail_support, $page_excerpt_support, $disable_wp_admin_top_bar, $clear_head_wp;


/*** FUNCTIONS **/
// Add Excerpts in Pages
function gd_cfg_addExcerptsInPagesAction(){
	add_post_type_support( 'page', 'excerpt' );
}


// Clear Tag Head	
function gd_cfg_clearHead(){
	remove_action( 'wp_head', 'feed_links' );
	remove_action( 'wp_head', 'rsd_link');
	remove_action( 'wp_head', 'wlwmanifest_link');
	remove_action( 'wp_head', 'index_rel_link');
	remove_action( 'wp_head', 'parent_post_rel_link');
	remove_action( 'wp_head', 'start_post_rel_link');
	remove_action( 'wp_head', 'adjacent_posts_rel_link');
	remove_action( 'wp_head', 'wp_generator');		
}	



/*** SETTINGS **/

// Show MySQL Errors
if($mysql_debug){
	ini_set( 'mysql.trace_mode', $mysql_debug);
}

// Show Thumbs 
if($post_thumbnail_support){
	add_theme_support( 'post-thumbnails' );
}

// Add Excerpts in Pages
if($page_excerpt_support){
	add_action( 'init', 'gd_cfg_addExcerptsInPagesAction' );
}

// Disable Admin Bar
if($disable_wp_admin_top_bar){
	add_filter('show_admin_bar','__return_false');	
}

// Clear Tag Head
if($clear_head_wp){
	gd_cfg_clearHead();
}