<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head>
 *
 * @package WordPress
 * @subpackage Gasoline.Digital.Theme
 * @since Gasoline.Digital.Theme 1.0
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta http-equiv="content-type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
        <meta http-equiv="content-language" content="<?php language_attributes(); ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name=developer content="@GasolineDigital | www.gasoline-digital.com"/>
        <meta name=web_author content="@GasolineDigital | www.gasoline-digital.com"/>
        <meta name=Custodian content="@GasolineDigital | www.gasoline-digital.com"/>

		<meta name="author" content="{#Owner Website Name#}"/>
        
        <title><?php gd_wp_title_simple(); ?></title>
		<meta name="description" content="<?php gd_wp_description();?>">        
		
		<!-- Facebook Meta Tags-->
		<meta property="og:type" content="activity" />
		<meta property="og:site_name" content="<?php bloginfo('name');?>" />
		<meta property="og:title" content="<?php gd_wp_title_simple(); ?>"/>
		<meta property="og:description" content="<?php gd_wp_description(); ?>"/>
		<meta property="og:image" content="<?php gd_wp_image(get_template_directory_uri().'img/logo.gif');?>"/>
		<meta property="og:url" content="<?php bloginfo('home');?>"/>

		<meta name="twitter:card" content="summary" />
	    <meta name="twitter:site" content="@gasolinedigital" />
	    <meta name="twitter:title" content="<?php gd_wp_title_simple(); ?>" />
	    <meta name="twitter:description" content="<?php gd_wp_description(); ?>" />
	    <meta name="twitter:image" content="<?php gd_wp_image(get_template_directory_uri().'img/logo.gif');?>" />
		
		<!-- Bootstrap -->
		<link href="<?php echo get_template_directory_uri()?>/css/bootstrap.css" rel="stylesheet">
		
		<!-- Template Style -->
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		    <![endif]-->

	
		<?php wp_head();?>

		<link rel="shortcut icon" href="<?php echo get_template_directory_uri()?>/favicon.ico" />		
	</head>
	<body>