<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head>
 *
 * @package WordPress
 * @subpackage Gasoline.Digital.Theme
 * @since Gasoline.Digital.Theme 1.0
 */
?>
<?php get_header(); ?>

<!-- Index -->	
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
	<h1><?php the_title();?></h1>
	<h2>Artigo</h2>
	<?php the_content();?>
	
<?php endwhile; endif;?>        		
<!-- // Index -->	

<?php get_footer(); ?>
