<?php
 /*
 *
 * Functions File
 *
 * @package WordPress
 * @subpackage Gasoline.Digital.Theme
 * @since Gasoline.Digital.Theme 1.0
 * @Updates: https://bitbucket.org/gasolinedeveloper/gasoline.digital.theme.blank
 */

/*
	Settings Default
*/
$mysql_debug 				= false; 	// MySQL Debug
$post_thumbnail_support 	= false; 	// Add Support Post Thumbnail
$page_excerpt_support 		= false; 	// Add Suport Excerpt In Pages
$clear_head_wp 				= true; 	// Clear Head WP
$disable_wp_admin_top_bar 	= true; 	// Disable WP-Admin Top Bar
$disable_wp_emoji		 	= true; 	// Disable WP Emoji
$disable_wp_comments 		= true; 	// Disable WP Native Comments 

// Include Gasoline.Digital Functions Theme
require_once('gasoline.digital/disable_emoji.php');
require_once('gasoline.digital/disable_comments.php');
require_once('gasoline.digital/general.php');
 
/**
 *  1. Filters
 *  2. Actions
 *
 *  4. General Functions  
 */

// #########  1.FILTERS


// #########  2.ACTIONS
/**
* Load Languages
*/
/*
add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup(){
    load_theme_textdomain('gasoline.digital.themes', get_template_directory() . '/languages');
}
*/

// ######## 4.General Functions


